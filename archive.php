<?php
    get_header();
?>

<div class="archive-sec">
    <div class="archive-left">
        <div class="section-title">
            <h2><?php single_cat_title();?></h2>
        </div>
        <div class="archive-box">
            <div class="container">
                <div class="post-box">
                    <?php
                        if(have_posts()):
                        while (have_posts()) : the_post();
                    ?>
                            <div class="post-box-tow-item">
                                <div class="news_logo_area">
                                    <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                        <?php
                                        if (has_post_thumbnail()) {
                                            the_post_thumbnail();
                                        }
                                        ?>
                                    </a>
                                    <h4>
                                        <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                        </a>
                                        <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                                    </h4>
                                    <h6>
                                        <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                        <br />
                                        <a href="<?php echo get_the_permalink();?>">Details</a>
                                    </h6>
                                </div>
                            </div>
                    <?php endwhile; endif; wp_reset_postdata(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer();?>
