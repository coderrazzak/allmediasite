<?php
    global $allmediasite;
?>

<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php bloginfo('title');?> | <?php bloginfo('description')?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="<?php echo $allmediasite['c-favicon']['url'];?>" type="image/x-icon">
    <link rel="icon" href="<?php echo $allmediasite['c-favicon']['url'];?>" type="image/x-icon">
    <!-- font awesome css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <meta name="theme-color" content="#fafafa">

    <?php wp_head();?>
</head>

<body  <?php body_class(); ?>>
<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->
<!-- header area -->
<div class="wrapper">
    <header class="header-area">
        <!-- logo area -->
        <div class="logo-area">
            <div class="siteLogo">
                <a href="<?php  bloginfo('home');?>">
                    <img src="<?php echo $allmediasite['c-logo']['url'];?>" alt="">
                </a>
            </div>
            <div class="top-ads">
                <img src="<?php echo $allmediasite['top-bar-ads']['url'];?>" alt="">
            </div>
        </div>


        <nav class="navigation-area">

            <div class="menu_w">
                <nav id="menu-wrap">
                    <?php wp_nav_menu(array(
                        'theme_location'=>'main-menu',
                        'menu_id'=>'menu',
                    ));?>
                </nav>
            </div>

        </nav>

    </header>