<?php

function enqueue_css_js(){
    wp_enqueue_style('normalize', get_template_directory_uri().'/css/normalize.css', array(), '8.0.1', 'all');
    wp_enqueue_style('font-awesome', get_template_directory_uri().'/css/font-awesome.min.css', array(), '4.7', 'all');

    wp_enqueue_style('main-css', get_template_directory_uri().'/css/main.css', array(), '1.0', 'all');

    wp_enqueue_style('style', get_template_directory_uri().'/style.css', array(), '1.0', 'all');

    wp_enqueue_style('responsive', get_template_directory_uri().'/css/responsive.css', array(), '1.0', 'all');
    wp_enqueue_style('mobile-menu', get_template_directory_uri().'/css/menu.css', array(), '1.0', 'all');


    wp_enqueue_script('modernizer', get_template_directory_uri().'/js/vendor/modernizr-3.7.1.min.js', array(), '3.7.1', 'true');
    wp_enqueue_script('plugins-js', get_template_directory_uri().'/js/plugins.js', array(), '1.0', 'true');
    wp_enqueue_script('mobile-menu', get_template_directory_uri().'/js/g', array(), '1.0', 'true');

    wp_enqueue_script('main-js', get_template_directory_uri().'/js/main.js', array(), '1.0', 'true');



}add_action('wp_enqueue_scripts', 'enqueue_css_js');