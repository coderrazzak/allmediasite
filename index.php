<?php get_header();?>

<!-- section one  -->
    <?php if($allmediasite['section-one'] == 1) {?>
        <section class="section-1">
        <?php
            $category_name = get_the_category_by_ID($allmediasite['cat-one']);
            $category_name_link = get_category_link($allmediasite['cat-one']);

        ?>
        <div class="section-title">
            <h2><?php echo $category_name;?></h2>
            <a href="<?php echo $category_name_link;?>">More</a>
        </div>
        <div class="container">
            <div class="post-box">
        <?php
        $how_post = $allmediasite['how_post_one'];

            $allmedia_site = new WP_Query(array(
                'post_type'     => 'post',
                'posts_per_page' => $how_post,
                'offset'        => 1,
                'category_name' => $category_name
            ));
            while($allmedia_site->have_posts()): $allmedia_site->the_post();
        ?>
            <div class="post">
                <div class="news_logo_area">
                    <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                        <?php
                        if (has_post_thumbnail()) {
                            the_post_thumbnail();
                        }
                        ?>
                    </a>
                    <h4>
                        <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                        </a>
                        <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                    </h4>
                    <h6>
                        <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                        <br />
                        <a href="<?php echo get_the_permalink();?>">Details</a>
                    </h6>
                </div>
            </div>
        <?php endwhile;?>
            </div>
        </div>
        </section>
    <?php }?>
<!-- end of section one PART 1 -->

<!-- section two  -->
<?php if($allmediasite['section-two'] == 1) {?>
    <section class="section-two">
        <div class="two-column">
        <div class="post-box-two">
          <div class="post-box-two-title">
              <?php
                $category_name = get_the_category_by_ID($allmediasite['cat-two']);
                $category_name_link = get_category_link($allmediasite['cat-two']);
              ?>
            <h2><?php echo $category_name;?></h2>
              <a href="<?php echo $category_name_link;?>">More</a>
          </div>

          <div class="container">
              <div class="post-box-tow-items">
                  <?php
                      $how_post = $allmediasite['how_post_two'];
                        $allmedia_site = new WP_Query(array(
                            'post_type'     => 'post',
                            'posts_per_page' => $how_post,
                            'offset'        => 0,
                            'category_name' => $category_name
                        ));
                        while($allmedia_site->have_posts()): $allmedia_site->the_post();
                  ?>

                  <div class="post-box-tow-item">
                      <div class="news_logo_area">
                          <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                              <?php
                              if (has_post_thumbnail()) {
                                  the_post_thumbnail();
                              }
                              ?>
                          </a>
                          <h4>
                              <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                              </a>
                              <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                          </h4>
                          <h6>
                              <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                              <br />
                              <a href="<?php echo get_the_permalink();?>">Details</a>
                          </h6>
                      </div>
                  </div>

                    <?php endwhile;?>
              </div>
          </div>
        </div>
        <div class="post-box-two">
            <?php
                $category_name = get_the_category_by_ID($allmediasite['cat-three']);
            ?>
        <div class="post-box-two-title">
          <h2><?php echo $category_name;?></h2>
            <a href="<?php echo $category_name_link;?>">More</a>
        </div>
        <div class="container two">
            <div class="post-box-tow-items">
            <?php
                $how_post = $allmediasite['how_post_three'];
                $allmedia_site = new WP_Query(array(
                    'post_type'     => 'post',
                    'posts_per_page' => $how_post,
                    'offset'        => 0,
                    'category_name' => $category_name
                ));
                while($allmedia_site->have_posts()): $allmedia_site->the_post();
            ?>
                <div class="post-box-tow-item">
                    <div class="news_logo_area">
                        <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                            <?php
                            if (has_post_thumbnail()) {
                                the_post_thumbnail();
                            }
                            ?>
                        </a>
                        <h4>
                            <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                            </a>
                            <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                        </h4>
                        <h6>
                            <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                            <br />
                            <a href="<?php echo get_the_permalink();?>">Details</a>
                        </h6>
                    </div>
                </div>
            <?php endwhile;?>

            </div>
        </div>
      </div>
  </div>
    </section>
<?php }?>
<!-- end of section two 1 -->

<!-- section three -->
    <?php if($allmediasite['section-three'] == 1) {?>
    <section class="section-1">
        <?php
        $category_name = get_the_category_by_ID($allmediasite['cat-four']);
        $category_name_link = get_category_link($allmediasite['cat-four']);

        ?>
        <div class="section-title">
            <h2><?php echo $category_name;?></h2>
            <a href="<?php echo $category_name_link;?>">More</a>
        </div>
        <div class="container">
            <div class="post-box">
                <?php
                $how_post = $allmediasite['how_post_four'];

                $allmedia_site = new WP_Query(array(
                    'post_type'     => 'post',
                    'posts_per_page' => $how_post,
                    'offset'        => 1,
                    'category_name' => $category_name
                ));
                while($allmedia_site->have_posts()): $allmedia_site->the_post();
                    ?>
                    <div class="post">
                        <div class="news_logo_area">
                            <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                <?php
                                if (has_post_thumbnail()) {
                                    the_post_thumbnail();
                                }
                                ?>
                            </a>
                            <h4>
                                <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                </a>
                                <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                            </h4>
                            <h6>
                                <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                <br />
                                <a href="<?php echo get_the_permalink();?>">Details</a>
                            </h6>
                        </div>
                    </div>
                <?php endwhile;?>
            </div>
        </div>
    </section>
<?php }?>
<!-- end of section three -->

<!-- section four -->
<?php if($allmediasite['section-four'] == 1) {?>
    <section class="section-two">
        <div class="two-column">
            <div class="post-box-two">
            <?php
                $category_name = get_the_category_by_ID($allmediasite['cat-five']);
                $category_name_link = get_category_link($allmediasite['cat-four']);
            ?>
                <div class="post-box-two-title">
                    <h2><?php echo $category_name;?></h2>
                    <a href="<?php echo $category_name_link;?>">More</a>
                </div>

                <div class="container two">
                    <div class="post-box-tow-items">
                        <?php
                        $how_post = $allmediasite['how_post_five'];
                        $allmedia_site = new WP_Query(array(
                            'post_type'     => 'post',
                            'posts_per_page' => $how_post,
                            'offset'        => 0,
                            'category_name' => $category_name
                        ));
                        while($allmedia_site->have_posts()): $allmedia_site->the_post();
                            ?>
                        <div class="post-box-tow-item">
                            <div class="news_logo_area">
                                <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                    <?php
                                    if (has_post_thumbnail()) {
                                        the_post_thumbnail();
                                    }
                                    ?>
                                </a>
                                <h4>
                                    <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                    </a>
                                    <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                                </h4>
                                <h6>
                                    <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                    <br />
                                    <a href="<?php echo get_the_permalink();?>">Details</a>
                                </h6>
                            </div>
                        </div>
                        <?php endwhile;?>
                    </div>
                </div>
            </div>
            <div class="post-box-two">
                <div class="post-box-two-title">
                    <?php
                    $category_name = get_the_category_by_ID($allmediasite['cat-six']);
                    $category_name_link = get_category_link($allmediasite['cat-six']);
                    ?>
                    <h2><?php echo $category_name;?></h2>
                    <a href="<?php echo $category_name_link;?>">More</a>
                </div>

                <div class="container">
                    <div class="post-box-tow-items">
                        <?php
                        $how_post = $allmediasite['how_post_six'];
                        $allmedia_site = new WP_Query(array(
                            'post_type'     => 'post',
                            'posts_per_page' => $how_post,
                            'offset'            => 0,
                            'category_name'     => $category_name
                        ));

                        while($allmedia_site->have_posts()): $allmedia_site->the_post();

                            ?>

                            <div class="post-box-tow-item">
                                <div class="news_logo_area">
                                    <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                        <?php
                                        if (has_post_thumbnail()) {
                                            the_post_thumbnail();
                                        }
                                        ?>
                                    </a>
                                    <h4>
                                        <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                        </a>
                                        <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                                    </h4>
                                    <h6>
                                        <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                        <br />
                                        <a href="<?php echo get_the_permalink();?>">Details</a>
                                    </h6>
                                </div>
                            </div>

                        <?php endwhile;?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }?>
<!-- end of section four -->

<!-- section five -->
<?php if($allmediasite['section-five'] == 1) {?>
    <section class="section-1">
        <?php
        $category_name = get_the_category_by_ID($allmediasite['cat-seven']);
        $category_name_link = get_category_link($allmediasite['cat-seven']);

        ?>
        <div class="section-title">
            <h2><?php echo $category_name;?></h2>
            <a href="<?php echo $category_name_link;?>">More</a>
        </div>
        <div class="container">
            <div class="post-box">
                <?php
                $how_post = $allmediasite['how_post_seven'];

                $allmedia_site = new WP_Query(array(
                    'post_type'     => 'post',
                    'posts_per_page' => $how_post,
                    'offset'        => 1,
                    'category_name' => $category_name
                ));
                while($allmedia_site->have_posts()): $allmedia_site->the_post();
                    ?>
                    <div class="post">
                        <div class="news_logo_area">
                            <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                <?php
                                if (has_post_thumbnail()) {
                                    the_post_thumbnail();
                                }
                                ?>
                            </a>
                            <h4>
                                <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                </a>
                                <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                            </h4>
                            <h6>
                                <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                <br />
                                <a href="<?php echo get_the_permalink();?>">Details</a>
                            </h6>
                        </div>
                    </div>
                <?php endwhile;?>
            </div>
        </div>
    </section>
<?php }?>
<!-- end of section five -->

<!-- section six -->
<?php if($allmediasite['section-six'] == 1) {?>
    <section class="section-two">
        <div class="two-column">
            <div class="post-box-two">
                <div class="post-box-two-title">
                    <?php
                    $category_name = get_the_category_by_ID($allmediasite['cat-eight']);
                    $category_name_link = get_category_link($allmediasite['cat-eight']);
                    ?>
                    <h2><?php echo $category_name;?></h2>
                    <a href="<?php echo $category_name_link;?>">More</a>
                </div>

                <div class="container">
                    <div class="post-box-tow-items">
                        <?php
                        $how_post = $allmediasite['how_post_eight'];
                        $allmedia_site = new WP_Query(array(
                            'post_type'     => 'post',
                            'posts_per_page' => $how_post,
                            'offset'        => 0,
                            'category_name' => $category_name
                        ));
                        while($allmedia_site->have_posts()): $allmedia_site->the_post();
                            ?>

                            <div class="post-box-tow-item">
                                <div class="news_logo_area">
                                    <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                        <?php
                                        if (has_post_thumbnail()) {
                                            the_post_thumbnail();
                                        }
                                        ?>
                                    </a>
                                    <h4>
                                        <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                        </a>
                                        <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                                    </h4>
                                    <h6>
                                        <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                        <br />
                                        <a href="<?php echo get_the_permalink();?>">Details</a>
                                    </h6>
                                </div>
                            </div>

                        <?php endwhile;?>
                    </div>
                </div>
            </div>
            <div class="post-box-two">
                <?php
                $category_name = get_the_category_by_ID($allmediasite['cat-nine']);
                $category_name_link = get_category_link($allmediasite['cat-eight']);
                ?>
                <div class="post-box-two-title">
                    <h2><?php echo $category_name;?></h2>
                    <a href="<?php echo $category_name_link;?>">More</a>
                </div>
                <div class="container two">
                    <div class="post-box-tow-items">
                        <?php
                        $how_post = $allmediasite['how_post_nine'];
                        $allmedia_site = new WP_Query(array(
                            'post_type'     => 'post',
                            'posts_per_page' => $how_post,
                            'offset'        => 0,
                            'category_name' => $category_name
                        ));
                        while($allmedia_site->have_posts()): $allmedia_site->the_post();
                            ?>
                            <div class="post-box-tow-item">
                                <div class="news_logo_area">
                                    <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                        <?php
                                        if (has_post_thumbnail()) {
                                            the_post_thumbnail();
                                        }
                                        ?>
                                    </a>
                                    <h4>
                                        <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                        </a>
                                        <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                                    </h4>
                                    <h6>
                                        <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                        <br />
                                        <a href="<?php echo get_the_permalink();?>">Details</a>
                                    </h6>
                                </div>
                            </div>
                        <?php endwhile;?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }?>
<!-- end of section six -->

<!-- section seven -->
<?php if($allmediasite['section-seven'] == 1) {?>
    <section class="section-1">
        <?php
        $category_name = get_the_category_by_ID($allmediasite['cat-ten']);
        $category_name_link = get_category_link($allmediasite['cat-ten']);

        ?>
        <div class="section-title">
            <h2><?php echo $category_name;?></h2>
            <a href="<?php echo $category_name_link;?>">More</a>
        </div>
        <div class="container">
            <div class="post-box">
                <?php
                $how_post = $allmediasite['how_post_ten'];
                $allmedia_site = new WP_Query(array(
                    'post_type'     => 'post',
                    'posts_per_page' => $how_post,
                    'offset'        => 1,
                    'category_name' => $category_name
                ));
                while($allmedia_site->have_posts()): $allmedia_site->the_post();
                    ?>
                    <div class="post">
                        <div class="news_logo_area">
                            <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                <?php
                                if (has_post_thumbnail()) {
                                    the_post_thumbnail();
                                }
                                ?>
                            </a>
                            <h4>
                                <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                </a>
                                <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                            </h4>
                            <h6>
                                <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                <br />
                                <a href="<?php echo get_the_permalink();?>">Details</a>
                            </h6>
                        </div>
                    </div>
                <?php endwhile;?>
            </div>
        </div>
    </section>
<?php }?>
<!-- end of section seven -->

<!-- section eight -->
<?php if($allmediasite['section-eight'] == 1) {?>
    <section class="section-two">
        <div class="two-column">
            <div class="post-box-two">
                <?php
                $category_name = get_the_category_by_ID($allmediasite['cat-eleven']);
                $category_name_link = get_category_link($allmediasite['cat-ten']);
                ?>
                <div class="post-box-two-title">
                    <h2><?php echo $category_name;?></h2>
                    <a href="<?php echo $category_name_link;?>">More</a>
                </div>

                <div class="container two">
                    <div class="post-box-tow-items">
                        <?php
                        $how_post = $allmediasite['how_post_eleven'];
                        $allmedia_site = new WP_Query(array(
                            'post_type'     => 'post',
                            'posts_per_page' => $how_post,
                            'offset'        => 0,
                            'category_name' => $category_name
                        ));
                        while($allmedia_site->have_posts()): $allmedia_site->the_post();
                            ?>
                            <div class="post-box-tow-item">
                                <div class="news_logo_area">
                                    <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                        <?php
                                        if (has_post_thumbnail()) {
                                            the_post_thumbnail();
                                        }
                                        ?>
                                    </a>
                                    <h4>
                                        <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                        </a>
                                        <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                                    </h4>
                                    <h6>
                                        <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                        <br />
                                        <a href="<?php echo get_the_permalink();?>">Details</a>
                                    </h6>
                                </div>
                            </div>
                        <?php endwhile;?>
                    </div>
                </div>
            </div>
            <div class="post-box-two">
                <div class="post-box-two-title">
                    <?php
                    $category_name = get_the_category_by_ID($allmediasite['cat-twelve']);
                    $category_name_link = get_category_link($allmediasite['cat-twelve']);
                    ?>
                    <h2><?php echo $category_name;?></h2>
                    <a href="<?php echo $category_name_link;?>">More</a>
                </div>

                <div class="container">
                    <div class="post-box-tow-items">
                        <?php
                        $how_post = $allmediasite['how_post_twelve'];
                        $allmedia_site = new WP_Query(array(
                            'post_type'     => 'post',
                            'posts_per_page' => $how_post,
                            'offset'            => 0,
                            'category_name'     => $category_name
                        ));

                        while($allmedia_site->have_posts()): $allmedia_site->the_post();

                            ?>

                            <div class="post-box-tow-item">
                                <div class="news_logo_area">
                                    <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                        <?php
                                        if (has_post_thumbnail()) {
                                            the_post_thumbnail();
                                        }
                                        ?>
                                    </a>
                                    <h4>
                                        <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                        </a>
                                        <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                                    </h4>
                                    <h6>
                                        <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                        <br />
                                        <a href="<?php echo get_the_permalink();?>">Details</a>
                                    </h6>
                                </div>
                            </div>

                        <?php endwhile;?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }?>
<!-- end of section eight-->

<!-- section nine -->
<?php if($allmediasite['section-nine'] == 1) {?>
    <section class="section-1">
        <?php
        $category_name = get_the_category_by_ID($allmediasite['cat-thirty']);
        $category_name_link = get_category_link($allmediasite['cat-thirty']);

        ?>
        <div class="section-title">
            <h2><?php echo $category_name;?></h2>
            <a href="<?php echo $category_name_link;?>">More</a>
        </div>
        <div class="container">
            <div class="post-box">
                <?php
                $how_post = $allmediasite['how_post_thirty'];

                $allmedia_site = new WP_Query(array(
                    'post_type'     => 'post',
                    'posts_per_page' => $how_post,
                    'offset'        => 1,
                    'category_name' => $category_name
                ));
                while($allmedia_site->have_posts()): $allmedia_site->the_post();
                    ?>
                    <div class="post">
                        <div class="news_logo_area">
                            <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                <?php
                                if (has_post_thumbnail()) {
                                    the_post_thumbnail();
                                }
                                ?>
                            </a>
                            <h4>
                                <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                </a>
                                <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                            </h4>
                            <h6>
                                <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                <br />
                                <a href="<?php echo get_the_permalink();?>">Details</a>
                            </h6>
                        </div>
                    </div>
                <?php endwhile;?>
            </div>
        </div>
    </section>
<?php }?>
<!-- end of section nine -->

<!-- section ten -->
<?php if($allmediasite['section-ten'] == 1) {?>
    <section class="section-two">
        <div class="two-column">
            <div class="post-box-two">
                <div class="post-box-two-title">
                    <?php
                    $category_name = get_the_category_by_ID($allmediasite['cat-fourty']);
                    $category_name_link = get_category_link($allmediasite['cat-fourty']);
                    ?>
                    <h2><?php echo $category_name;?></h2>
                    <a href="<?php echo $category_name_link;?>">More</a>
                </div>

                <div class="container">
                    <div class="post-box-tow-items">
                        <?php
                        $how_post = $allmediasite['how_post_fourty'];
                        $allmedia_site = new WP_Query(array(
                            'post_type'     => 'post',
                            'posts_per_page' => $how_post,
                            'offset'        => 0,
                            'category_name' => $category_name
                        ));
                        while($allmedia_site->have_posts()): $allmedia_site->the_post();
                            ?>

                            <div class="post-box-tow-item">
                                <div class="news_logo_area">
                                    <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                        <?php
                                        if (has_post_thumbnail()) {
                                            the_post_thumbnail();
                                        }
                                        ?>
                                    </a>
                                    <h4>
                                        <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                        </a>
                                        <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                                    </h4>
                                    <h6>
                                        <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                        <br />
                                        <a href="<?php echo get_the_permalink();?>">Details</a>
                                    </h6>
                                </div>
                            </div>

                        <?php endwhile;?>
                    </div>
                </div>
            </div>
            <div class="post-box-two">
                <?php
                $category_name = get_the_category_by_ID($allmediasite['cat-fivty']);
                $category_name_link = get_category_link($allmediasite['cat-fourty']);
                ?>
                <div class="post-box-two-title">
                    <h2><?php echo $category_name;?></h2>
                    <a href="<?php echo $category_name_link;?>">More</a>
                </div>
                <div class="container two">
                    <div class="post-box-tow-items">
                        <?php
                        $how_post = $allmediasite['how_post_fivty'];
                        $allmedia_site = new WP_Query(array(
                            'post_type'     => 'post',
                            'posts_per_page' => $how_post,
                            'offset'        => 0,
                            'category_name' => $category_name
                        ));
                        while($allmedia_site->have_posts()): $allmedia_site->the_post();
                            ?>
                            <div class="post-box-tow-item">
                                <div class="news_logo_area">
                                    <a href="<?php echo get_post_meta(get_the_ID(), 'website-url', true);?>" target="_blank">
                                        <?php
                                        if (has_post_thumbnail()) {
                                            the_post_thumbnail();
                                        }
                                        ?>
                                    </a>
                                    <h4>
                                        <a href="<?php echo get_post_meta(get_the_ID(),'two',true);?>" target="_blank">
                                        </a>
                                        <?php echo get_post_meta(get_the_ID(), 'website-name', true);?>
                                    </h4>
                                    <h6>
                                        <?php echo get_post_meta(get_the_ID(), 'website-address', true);?>
                                        <br />
                                        <a href="<?php echo get_the_permalink();?>">Details</a>
                                    </h6>
                                </div>
                            </div>
                        <?php endwhile;?>

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }?>
<!-- end of section ten -->

<?php get_footer();?>
