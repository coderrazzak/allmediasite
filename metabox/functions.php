<?php

    function create_our_metabox(array $ourmetabox){
        $ourmetabox[] = array(
            'id'    => 'first-section',
            'title' => 'This meta box contain website links and address',
            'object_types' => array('post'),
            'fields'    => array(
                array(
                    'id'    => 'website-name',
                    'name'  => 'Enter Website Name',
                    'type'  => 'text',
                ),
                array(
                    'id'    => 'website-url',
                    'name'  => 'Enter Website URL',
                    'type'  => 'text_url',
                ),
                array(
                    'id'    => 'website-address',
                    'name'  => 'Enter Website Address',
                    'type'  => 'textarea_small'
                ),
            )
        );
        return $ourmetabox;
    }

    add_filter('cmb2_meta_boxes', 'create_our_metabox');