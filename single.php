<?php
/**
 * The template for displaying all single posts
 *
 * @link
 *
 * @package WordPress
 * @subpackage allmediasite
 * @since 1.0
 * @version 1.0
 */
    get_header();
?>

<div class="container">

    <div class="single-post-con">
        <div class="meta-area">
            <div class="meta-area-headline">
                <a href="<?php echo get_the_permalink();?>">
                    <h2><?php echo get_the_title();?></h2>
                </a>
            </div>
            <div class="meta-area-post">
                <a href="<?php echo get_the_permalink();?>" class="post-img-box">
                    <?php echo get_the_post_thumbnail();?>
                </a>
                <h4>
                    <?php
                        echo get_post_meta(get_the_ID(), 'website-name', true);
                    ?>
                </h4>
                <address>
                    <?php
                        echo get_post_meta(get_the_ID(), 'website-address', true);
                    ?>
                    <br>
                    <hr>
                    <br>
                </address>
            </div>
        </div>
        <div class="content-area">
            <?php
            if (have_posts()) :
                while (have_posts()) : the_post();

                    the_content();

                endwhile;
            endif;
            ?>
        </div>
    </div>
<?php //endwhile; endif; wp_reset_postdata(); ?>

</div>

<br>

<?php get_footer();?>

